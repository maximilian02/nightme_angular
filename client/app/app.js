'use strict';
NG_APP_ENV = window.NG_APP_ENV || {};

var nightMeApp = angular.module('nightMeApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/events', {
        templateUrl: 'app/events/events.html',
        controller: 'EventsCtrl'
      })
      //Events
      .when('/admin/events', {
        templateUrl: 'app/admin/events/events.html',
        controller: 'AdminEventsCtrl'
      })
        .when('/admin/events/create', {
          templateUrl: 'app/admin/events/create/create_event.html',
          controller: 'AdminCreateEventsCtrl'
        })
        .when('/admin/events/update/:eventId', {
          templateUrl: 'app/admin/events/update/update_event.html',
          controller: 'AdminUpdateEventsCtrl'
        })
      //Artists
      .when('/admin/artists', {
        templateUrl: 'app/admin/artists/artists.html',
        controller: 'AdminArtistsCtrl'
      })
        .when('/admin/artists/create', {
          templateUrl: 'app/admin/artists/create/create_artist.html',
          controller: 'AdminCreateArtistsCtrl'
        })
        .when('/admin/artists/update/:artistId', {
          templateUrl: 'app/admin/artists/update/update_event.html',
          controller: 'AdminUpdateArtistsCtrl'
        })  
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });