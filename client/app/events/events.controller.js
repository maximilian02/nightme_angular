'use strict';

nightMeApp
  .controller('EventsCtrl', function ($scope, $http, APIService) {
    $scope.events = [];

    $scope.getAllEvents = function() {
      APIService.getData('event')
        .success(function (res) {
          console.log(res);
          for (var i = 0; i < res.length; i++) {
            var date = new Date(res[i].date);
            var day = date.getDate() <= 9 ? '0' + date.getDate() : date.getDate();
            var month = (1 + date.getMonth());
            month = month <= 9 ? '0' + month : month;

            res[i].date = day + '/' + month + '/' + date.getFullYear();

          };

          $scope.events = res;

        })
        .error(function (err) {
          console.log(err);
        });
    };

    $scope.getAllEvents();
  });
