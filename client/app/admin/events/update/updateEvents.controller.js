'use strict';

nightMeApp
  .controller('AdminUpdateEventsCtrl', function ($scope, $http, $location, $routeParams, APIService) {
    $scope.artists = [];
    $scope.eventData = {};
    $scope.places = [];
    $scope.selectedPlace = {};
    $scope.eventId = $routeParams.eventId;
    console.log('$scope.eventId', $scope.eventId);

    $scope.$watch('selectedPlace', function(newValue, oldValue) {
      console.log('newValue', newValue);
    });

    $scope.getEventData = function() {
      APIService.getData('event', $scope.eventId)
        .success(function (res) {
          console.log('Event', res);
          $scope.eventData = res[0];
          var date = new Date($scope.eventData.date);
          var day = date.getDate() <= 9 ? '0' + date.getDate() : date.getDate();
          var month = (1 + date.getMonth());
          month = month <= 9 ? '0' + month : month;
          $scope.eventData.date = day + '/' + month + '/' + date.getFullYear();

          //Detecting artists to select
          for (var i = 0; i < $scope.eventData._artists.length; i++) {
            var _ar = $scope.eventData._artists[i];
            for (var j = 0; j < $scope.artists.length; j++) {
              if (_ar._id === $scope.artists[j]._id) {
                $scope.artists[j].selected = true;
              }
            };
          };

          //Detecting places to select
          for (var k = 0; k < $scope.places.length; k++) {
            if($scope.eventData._place._id === $scope.places[k]._id) {
              $scope.places[k].selected = true;
            }
          };

        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    }

    $scope.getAllArtists = function() {
      APIService.getData('artist')
        .success(function (res) {
          $scope.artists = res;
          $scope.getAllPlaces();
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.getAllPlaces = function() {
      APIService.getData('place')
        .success(function (res) {
          $scope.places = res;
          $scope.getEventData();
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };
    
    $scope.updateEvent = function(u) {
      var splitDate = $scope.eventData.date.split('/');
      var dateSetUp = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
      var dateObj = new Date(dateSetUp);

      console.log('updtEvent', u);

      var cfg = {
        title: $scope.eventData.title,
        date: dateObj.toString('yyyy-MM-dd'),
        artists: u.selectedArtist,
        place: u.selectedPlace
      };

      console.log(cfg);

    //   APIService.updateData('event', cfg)
    //     .success(function (res) {
    //       if (res.status) {
    //         $scope.success = true;
    //       } else {
    //         $scope.error = true;  
    //       }
    //       console.log(res);
    //     })
    //     .error(function (err) {
    //       $scope.error = true;
    //       console.log(err);
    //     });
    };

    $scope.getAllArtists();
  });
