'use strict';

nightMeApp
  .controller('AdminCreateEventsCtrl', function ($scope, $http, $location, APIService) {
    $scope.artists = [];
    $scope.places = [];
    $scope.newEvent = {};
    $scope.error = false;
    $scope.success = false;

    $scope.getAllArtists = function() {
      APIService.getData('artist')
        .success(function (res) {
          console.log(res);
          $scope.artists = res;
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.getAllPlaces = function() {
      APIService.getData('place')
        .success(function (res) {
          console.log(res);
          $scope.places = res;
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.createEvent = function(newEvent) {
      console.log(newEvent);
      var splitDate = newEvent.eventDate.split('/');
      var dateSetUp = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
      var dateObj = new Date(dateSetUp);

      var cfg = {
        title: newEvent.eventTitle,
        date: dateObj.toString('yyyy-MM-dd'),
        artists: newEvent.selectedArtist,
        place: newEvent.selectedPlace
      };

      APIService.createData('event', cfg)
        .success(function (res) {
          if (res.status) {
            $scope.success = true;
          } else {
            $scope.error = true;  
          }
          console.log(res);
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.getAllPlaces();
    $scope.getAllArtists();
  });
