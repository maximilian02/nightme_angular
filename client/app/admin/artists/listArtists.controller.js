'use strict';

nightMeApp
  .controller('AdminArtistsCtrl', function ($scope, $http, $location, APIService) {
    $scope.artists = [];

    $scope.getAllArtists = function() {
      APIService.getData('artist')
        .success(function (res) {
          console.log(res);
          $scope.artists = res;
        })
        .error(function (err) {
          console.log(err);
        });
    };

    $scope.deleteArtist = function(artistId) {
      var value = confirm('Are you really sure you want to delete that?');
      if(value) {
        APIService.deleteData('artist', artistId)
          .success(function (res) {
            $scope.getAllArtists();
          })
          .error(function (err) {
            console.log(err);
          });
      }
    };

    $scope.getAllArtists();
  });
