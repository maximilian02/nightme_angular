'use strict';

nightMeApp
  .controller('AdminCreateArtistsCtrl', function ($scope, $http, $location, APIService) {
    $scope.newArtist = {};
    $scope.musicGenres = [];
    $scope.selected = [];
    $scope.selectedGenres = [];
    $scope.error = false;
    $scope.success = false;

    $scope.getAllGenres = function() {
      APIService.getData('musicgenre')
        .success(function (res) {
          console.log(res);
          $scope.musicGenres = res;

        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.createArtist = function(newArtist) {
      var cfg = {
        name: newArtist.artistName,
        website: newArtist.artistWebsite
      };

      if (newArtist.selectedGenres && newArtist.selectedGenres.length > 0) {
        cfg.genre = newArtist.selectedGenres[0];
      }

      APIService.createData('artist', cfg)
        .success(function (res) {
          console.log(res)
          if (res.status) {
            $scope.success = true;
          } else {
            $scope.error = true;  
          }
        })
        .error(function (err) {
          $scope.error = true;
          console.log(err);
        });
    };

    $scope.getAllGenres();
  });
