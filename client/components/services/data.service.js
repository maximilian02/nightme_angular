'use strict';

nightMeApp  
  .factory('APIService', function( $resource, $http) {
    var API = {};
        API.baseUrl = NG_APP_ENV.api + 'api/',
        API.requestUrl = '',
        API.entity = '';

    API.eventsRequest = function(method, data) {
      var config = {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + NG_APP_ENV.authToken
        }
      };

      // DEV var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NTVmZTUxNzllNGE3MGIzMDMzMjNlM2EiLCJ1c2VybmFtZSI6Im1heCIsImhhc2hlZFBhc3N3b3JkIjoiY2FjOTdlOWU4MTZkNzJkYTkwY2U3NTA3ZDkwYmE5MzIxMGM0MzVlOCIsInNhbHQiOiJmOGM3M2I3MmRkZTM3ZmVmMzE4MGI4NWVlYjkxY2RhNjlhYjIyODY4OThjNWZhN2Q1ODc4ODAxN2M2NDE1MDVmIiwiZW1haWwiOiJtYXguel9AaG90bWFpbC5jb20iLCJfX3YiOjAsIl9hcnRpc3RzIjpbXSwiY3JlYXRlZCI6IjIwMTUtMDUtMjNUMDI6MjU6MjYuOTkzWiJ9.oW4vm5W-kjlKp0qjUUhuf5XFVAq7yhhlnO2JJm5bA-E";
      var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NTYxNGNlNmU0YjBiNDcxYjNkNTdkZjciLCJ1c2VybmFtZSI6Im1heCIsImhhc2hlZFBhc3N3b3JkIjoiY2FjOTdlOWU4MTZkNzJkYTkwY2U3NTA3ZDkwYmE5MzIxMGM0MzVlOCIsInNhbHQiOiJmOGM3M2I3MmRkZTM3ZmVmMzE4MGI4NWVlYjkxY2RhNjlhYjIyODY4OThjNWZhN2Q1ODc4ODAxN2M2NDE1MDVmIiwiZW1haWwiOiJtYXguel9AaG90bWFpbC5jb20iLCJfYXJ0aXN0cyI6W10sImNyZWF0ZWQiOiIyMDE1LTA1LTI0VDA0OjAwOjQ4LjA2NVoifQ.pX-3yh1jLcnH-L5L8uqLZg211mNVtwExWvdNMNSFGqo";
      
      if( 
          API.entity === 'user' || API.entity === 'artist' || 
          API.entity === 'user' || API.entity === 'musicgenre' || 
          API.entity === 'provincestate' || API.entity === 'country' || 
          API.entity === 'place' ||  API.entity === 'comment'
      ){
        config.params = {
          token: token
        };
      }

      console.log(config);

      switch(method){
        case 'GET':
          return $http.get(this.requestUrl, config);
          break;
        case 'POST':
          return $http.post(this.requestUrl, data, config);
          break;
        case 'PUT':
          return $http.put(this.requestUrl, data, config);
          break;
        case 'DELETE':
          return $http.delete(this.requestUrl, config);
          break;
      }
    };

    API.getData = function (entity, dataId) {
      this.requestUrl = dataId ? this.baseUrl + entity + '/' + dataId : this.baseUrl + entity;
      this.entity = entity;
      return this.eventsRequest('GET');
    };

    API.createData = function (entity, data) {
      this.requestUrl = this.baseUrl + entity;
      return this.eventsRequest('POST', data);
    };

    API.upateData = function (entity, data) {
      this.requestUrl = this.baseUrl + entity + '/' + eventId;
      return this.eventsRequest('PUT', data);
    };

    API.deleteData = function (entity, eventId) {
      this.requestUrl = this.baseUrl + entity + '/' + eventId;
      return this.eventsRequest('DELETE');
    }

    return API;
  });
