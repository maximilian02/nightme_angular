'use strict';

nightMeApp
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.menu = [{
      'title': 'Coming soon',
      'link': '/events'
    }];

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });